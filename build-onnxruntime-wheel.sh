#!/bin/bash

dev_install () {
    yum -y update
    yum -y upgrade
}

python_packages () {
    cd /home/
    rm -rf venv
    python3.8 -m venv venv
    source venv/bin/activate
    python -m pip install --upgrade pip
    pip install wheel numpy auditwheel
}

install_onnx_and_create_wheel () {
    git clone --depth 1 --recursive --branch v1.9.1 https://github.com/microsoft/onnxruntime
    patch onnxruntime/setup.py /outputs/patches/setup.patch
    cd onnxruntime

    ./build.sh \
        --config Release \
        --update \
        --build \
        --build_shared_lib \
        --enable_pybind \
        --build_wheel \
        --use_dnnl \
        --parallel 8 \
        --skip_tests

    cd build/Linux/Release/dist/
    wheel_file=$(find . -name '*.whl')

    cp $wheel_file /outputs/
    cd /outputs/
    auditwheel repair $wheel_file
}

main () {
    dev_install
    python_packages
    install_onnx_and_create_wheel
}

main