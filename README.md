# onnxruntime-for-lambda

Docker script for building [ONNX Runtime](https://github.com/microsoft/onnxruntime) Python 3.8 wheel package with [oneDNN](https://github.com/oneapi-src/oneDNN) execution provider in [manylinux](https://github.com/pypa/manylinux) environment. This means that the wheel packages is also compatible for an AWS Lambda layer.

There is an AWS CodeBuild script to build and push the Python wheel package to AWS CodeArtifact.

## Build requirements

- Docker

## Building

Have Docker running and then to simply build the ONNX Runtime wheel package:

```bash
make build_package
```

## Different python versions

This script will facilitate newer Python versions in the future, such as Python 3.9.

