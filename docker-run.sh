#!/bin/bash

docker run \
    --cpus=8 \
    --memory=10g \
    -v $(pwd):/outputs \
    -i quay.io/pypa/manylinux2014_x86_64 \
    /bin/bash \
    outputs/build-onnxruntime-wheel.sh